import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

//4/6
public class TestCountsProjectAndYearImpl {

	CountsProjectAndYearImpl countsProjectAndYear = null;
	
	@Before
	public void setup() {
		//the class we are testing, extnesiate the class so we can use those methods in the class
		countsProjectAndYear = new CountsProjectAndYearImpl();
	}
	
	@Test //1. Test when both operands are null
	public void testComposeResponseWhenBothOperatorsNull() throws Exception {
		String ret = countsProjectAndYear.composeResponse(null, null);
		assertEquals("Invalid", ret);
	}
	
	@Test //2. Test when when project is null
	public void testComposeResponseWhenProjectsNull() throws Exception {
		String ret = countsProjectAndYear.composeResponse(null, "2015");
		assertEquals("Required parameter < null > missing", ret);		
	}
	
	@Test //3. Test when when year is null
	public void testComposeResponseWhenYearNull() throws Exception {
		String ret = countsProjectAndYear.composeResponse("heat", null);
		assertEquals("Required parameter < null > missing", ret);		
	}
	
	@Test //4. Test when when project is invalid
	public void testComposeResponseWhenInvalidProject() throws Exception {
		String mockProject = "Patrizio";
		String mockYear = "2015";
		Elements links = null;
		String source = "http://eavesdrop.openstack.org/meetings/" + mockProject;
		JSoupHandler jsoupHandler = mock(JSoupHandler.class);
		when(jsoupHandler.getElements(source)).thenReturn(links);
		String response = countsProjectAndYear.composeResponse(mockProject, mockYear);
		assertEquals("ERROR: Project with < Patrizio > not found", response); //"Invalid year < heat > for project < 1787 >"		
	}
	
	@Test //5. Test when when year is invalid
	public void testComposeResponseWhenInvalidYear() throws Exception{
		String mockProject = "heat";
		String mockYear = "1787";
		Elements links = null;
		String source = "http://eavesdrop.openstack.org/meetings/" + mockProject + "/" + mockYear;
		JSoupHandler jsoupHandler = mock(JSoupHandler.class);
		when(jsoupHandler.getElements(source)).thenReturn(links);
		String response = countsProjectAndYear.composeResponse(mockProject, mockYear);
		assertEquals(null, response); //"ERROR: Invalid year < heat > for project < 1787 >"	
	}
	
}

















