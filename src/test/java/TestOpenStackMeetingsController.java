import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestOpenStackMeetingsController {

	OpenStackMeetingsController openStackMeetingsController = new OpenStackMeetingsController();
	OpenStackMeetingsService mockService = null;
	
	@Before
	public void setUp(){
		mockService = mock(OpenStackMeetingsService.class);
		openStackMeetingsController.setCountsProjectAndYear(mockService);
	}
	
	@Test //6
	public void testHelloWorld(){
		String reply = openStackMeetingsController.helloWorld();
		assertEquals("Welcome to OpenStack meeting statistics calculation page. Please provide project and year as query parameters.", reply);
	}
	
	@Test //7
	public void testGetCount() throws Exception{
		when(mockService.composeResponse("heat","2016")).thenReturn("145");
		String reply = openStackMeetingsController.getCount("heat","2016");
		String mockResponse =  reply;
		assertEquals(" Number of Files: 145", mockResponse); 
	}
	
	@Test //8
	public void testGetCountNull() throws Exception{
		when(mockService.composeResponse("projectNull","yearNull")).thenReturn("null");
		String reply = openStackMeetingsController.getCount("hello","1999");
		String mockResponse =  reply;
		assertEquals(" ERROR: Cannot Accept Null Parameters", mockResponse); 
	}
	
}
