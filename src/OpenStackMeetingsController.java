
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//<servlet-class>assignment2.OpenStackMeetingsController</servlet-class>
//entry point of application
//in spring you need to specify URLs, and they should be mapped
//those methods within the mapped that are going to be executed will be on the controller class
//service classes are implemented for the business logic (very generic) 
//think of it as: make a calls to the evedrop element and doing the count stuff. this should be done in the service class
//you should define interfaces, and implementation as well in the service class. 
//we should have interfaces to swap out implementations 

@Controller
public class OpenStackMeetingsController {
	
	private OpenStackMeetingsService countsProjectAndYear;
	
	public OpenStackMeetingsController() {
		
	}

	
	public  OpenStackMeetingsController(OpenStackMeetingsService countService) {
		this.countsProjectAndYear = countService;
	}
	
	@ResponseBody
    @RequestMapping(value = "/")
    public String helloWorld()
    {
        return "Hello world!";
    }
	
	
//	@ResponseBody
//	@RequestMapping(value = "/email") 
//	public String getNumber(@RequestParam("project") String language, @RequestParam("year") Integer number)
//	{
//		if (language.equalsIgnoreCase("English")) {
//			return englishEditorService.getName() + " " + englishEditorService.composeEmail();
//		}
//		else if (language.equalsIgnoreCase("Spanish")) {
//			return spanishEditorService.getName() + " " + spanishEditorService.composeEmail();
//		}
//		else {
//			return "Language " + language + " not supported.";
//		}
//	}
	
	
}
