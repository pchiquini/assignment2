

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.ExceptionHandler;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

//import assignment1.JSoupHandler;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


//<servlet-class>assignment2.OpenStackMeetingsController</servlet-class>
//entry point of application
//in spring you need to specify URLs, and they should be mapped
//those methods within the mapped that are going to be executed will be on the controller class
//service classes are implemented for the business logic (very generic) 
//think of it as: make a calls to the evedrop element and doing the count stuff. this should be done in the service class
//you should define interfaces, and implementation as well in the service class. 
//we should have interfaces to swap out implementations 

@Controller
public class OpenStackMeetingsController {
	
	private OpenStackMeetingsService countsProjectAndYear;

	public  void setCountsProjectAndYear(OpenStackMeetingsService countsProjectAndYear) {
		this.countsProjectAndYear = countsProjectAndYear;
	}
	
	@ResponseBody
    @RequestMapping(value = "/openstackmeetings")
    public String helloWorld()
    {
        return "Welcome to OpenStack meeting statistics calculation page. Please provide project and year as query parameters.";
    }
	
	@ResponseBody
    @RequestMapping(value = "/openstackmeetings", params = {"project", "year" }, method=RequestMethod.GET)
    public String getCount(@RequestParam("project") String project, @RequestParam("year") String year) throws Exception
    {
			String totalCount = "";
			totalCount = countsProjectAndYear.composeResponse(project, year);
			
			if(totalCount == null)
				return " ERROR: Cannot Accept Null Parameters";
			else if(!totalCount.isEmpty() && totalCount.contains("ERROR"))
				return totalCount;
			else
				return " Number of Files: " + totalCount;
	}
}


