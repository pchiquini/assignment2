


public interface OpenStackMeetingsService {

	public String composeEmail();
	
	public String composeResponse(String project, String year) throws Exception;
	
	public String getCount();
	
}
