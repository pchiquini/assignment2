

public class ProjectException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * @param   message  the detail message.
     */
	    public ProjectException(String message) {
	        super(message);
	    }
}

