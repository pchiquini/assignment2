

import java.io.PrintWriter;
import java.util.ListIterator;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CountsProjectAndYearImpl implements OpenStackMeetingsService {

	JSoupHandler jsoupHandler;
	//List<String> numResp = new ArrayList<String>();
	
	public CountsProjectAndYearImpl() {
		
		super();
		if (jsoupHandler == null) {
			jsoupHandler = new JSoupHandler();
		}
		
	}

	public String getCount() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String composeResponse(String project, String year) throws Exception{
		
		if(project != null && year != null && !project.isEmpty() && !year.isEmpty() ){
	
			try {	
				Elements links;
				String source = "http://eavesdrop.openstack.org/meetings/" + project;
				Elements links2 = jsoupHandler.getElements(source);
			
				if(links2 == null){
					return "ERROR: Project with < " + project + " > not found";
			    }
				
				else {
					String source2 = "http://eavesdrop.openstack.org/meetings/" + project + "/" + year;
					links = jsoupHandler.getElements(source2).select("td:nth-child(2)");	
				}
				
				if (links != null) {
				    ListIterator<Element> iter = links.listIterator();	 
				    iter.next();
				    
				    while(iter.hasNext()) {
			    			Element e = (Element) iter.next();
			    			String link = e.text();
				    }	
				    
				    int count = links.size()-1;
				    return Integer.toString(count);
			    }

			    else {
			    	return "ERROR: Invalid year < " + year + " > for project < " + project + " >";
			    }
				
			} catch (Exception exp) {
				exp.printStackTrace();
			}	
	}
		else{ //throw new KeywordNotFoundException("Required parameter <name> missing");
			String response = "Required parameter < "+ project + " > missing";
			String response2 = "Required parameter < "+ year + " > missing";
			String response3 = "Invalid";
			if(project == null && year == null){
				return response3;
			}
			else if(year == null)
				return response2;
			
			else if(project == null)
				return response;
		}
						
	return null;
		
	}
	
	public String composeEmail() {
		return "Hello world.";
	}
	
	@ExceptionHandler({ProjectException.class})
	 public ModelAndView myProjectError(Exception exception) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("Unkown Project");
	    mav.addObject("Project with <  project  > not found");
	    return mav;
	    
	}
	
	@ExceptionHandler({YearException.class})
	public ModelAndView myYearError(Exception exception) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("Unkown Year");
	    mav.addObject("Invalid year < year > for project < project >");
	    return mav;
	}

}
