
public class YearException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * @param   message   the detail message.
     */
	public YearException(String message) {
        super(message);
    }

}