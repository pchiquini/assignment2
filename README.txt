Name: Patrizio Chiquini  
EID: pc22566  
Notes: Discussion/Notes about the assignment (if you have any)

- - - - - Assignment 2 - - - - - -

Objective:
In this assignment you will learn about Spring framework and unit testing.


Introduction:
In this assignment we will build on the previous assignment and calculate the total number of files generated corresponding to meetings that happened for a particular project in a particular year. Similar to assignment 1, you will query the eavesdrop website (http://eavesdrop.openstack.org/meetings) for obtaining the meeting data.

You do not need to implement sessions in this assignment.


Details:
You will implement a Spring controller that will handle two query parameters: 
- project - year
The project and the year parameters will be used for determining what data should be shown in response to a request.

Query parameter: 
project			Names of projects from eavesdrop site
			Should not be treated as case sensitive 
			(solum, Solum, soLuM, etc. should be treated as same)

year			Year numbers from eavesdrop site
			Only digits are allowed 
			(2016 is correct, twentysixteen is incorrect)


 Summary of requirements:
 Use Spring 
 Create Controller and Service classes 
 Use constructor or setter injection to wire controller and service classes together
 Define beans in servletContext.xml
 Write at least 5 unit tests between controller and service classes 
 Include README as part of your submission





